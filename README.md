# qssi-user 13 TP1A.220905.001 1671283369422 release-keys
- manufacturer: qualcomm
- platform: kona
- codename: kona
- flavor: qssi-user
- release: 13
- id: TP1A.220905.001
- incremental: 1671283369422
- tags: release-keys
- fingerprint: qti/kona/kona:12/RKQ1.211119.001/1671793275967:user/release-keys
- is_ab: false
- brand: qti
- branch: qssi-user-13-TP1A.220905.001-1671283369422-release-keys
- repo: qti_kona_dump
